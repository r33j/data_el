setwd("~/projects/data_el/csv/rainetteformated/")
#replika values before patch
r_before <- read.csv(file = '10062020.csv', 
                     sep ='|', 
                     header = FALSE,
                     encoding = "UTF-8",
                     row.names = NULL)

wbcCorpus <- lapply(r_before, iconv, to = "ASCII")
cleanCorpus <- lapply(wbcCorpus, 
                      removePunctuation,    
                      preserve_intra_word_contractions = TRUE,
                      preserve_intra_word_dashes = TRUE,)


r_corp<-Corpus(VectorSource(cleanCorpus$V3))




#Segmenting our values
corpus <- split_segments(r_corp, segment_size = 20)

dtm <- dfm(corpus, remove = stopwords("en"), tolower = TRUE)
dtm <- dfm_wordstem(dtm, language = "english")
dtm <- dfm_trim(dtm, min_termfreq = 3)

res <- rainette(dtm, k = 6, min_uc_size = 15, min_split_members = 20)

rainette_explor(res, dtm)

