import pandas as pd
import moment
from datetime import datetime


today = moment.now().format("DDMMYYYY")

df= pd.read_json(r'./json/'+today+'_allreviews.json')

to_csv= df.to_csv(r'../csv/allreviews/'+today+'_allreviews.csv', encoding='utf-8', sep='|', header= True)

f=pd.read_csv('../csv/allreviews/'+today+'_allreviews.csv', sep='|')
keep_col = ['date','text']
new_f = f[keep_col]
new_f.to_csv("../csv/rainetteformated/"+today+".csv", encoding="utf-8", sep='|', header= True)
print("All done chief !")


